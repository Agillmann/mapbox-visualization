import { v4 as uuid } from 'uuid';

export default async function (data) {
  let request = new URL('https://api-adresse.data.gouv.fr/search/'),
    params = { q: data.data[0], postcode: data.data[1], limit: 1 };
  Object.keys(params).forEach((key) => request.searchParams.append(key, params[key]));
  // For add params q : address and postcode : postcode
  const fetchResult = await fetch(request); //Making the req
  const result = await fetchResult.json(); // parsing the response
  if (fetchResult.ok) {
    const id = uuid();
    const long = result.features[0].geometry.coordinates[0];
    const lat = result.features[0].geometry.coordinates[1];
    const address = result.features[0].properties.name;
    const postcode = result.features[0].properties.postcode;
    return { id, address, postcode, lat, long, data }; // return success object
  }

  const responseError = {
    type: 'Error',
    message: result.message || 'Something went wrong',
    data: result.data || '',
    code: result.code || '',
  };

  const error = new Error('Obligatoire');
  error.info = responseError;
  return error;
}
