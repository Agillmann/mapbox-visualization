import React, { useContext } from 'react';
import './App.css';
import { LocationDataContext } from './GlobalState';
import Map from './components/Map';
import Dropzone from './components/Dropzone';
import Filters from './components/Filters';
import Grid from '@material-ui/core/Grid';

function App() {
  const [state] = useContext(LocationDataContext);
  return (
    <Grid container spacing={0}>
      <Grid item xs={10}>
        <Map></Map>
      </Grid>
      <Grid item xs={2}>
        <section className="info">
          <Filters></Filters>
          <Dropzone></Dropzone>
          <p className="info__update">
            Dernière mise à jour : <span>{state.lastUpdate}</span>
          </p>
          <p className="info__item">En construction : 10</p>
          <p className="info__item">En attente : 15</p>
          <p className="info__item">Fini : 50</p>
        </section>
      </Grid>
    </Grid>
  );
}

export default App;
