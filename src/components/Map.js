import React, { useState, useContext } from 'react';
import ReactMapGl from 'react-map-gl';
import MarkerDialog from './MarkerDialog';

import { LocationDataContext } from '../GlobalState';

const DEFAULT_VIEWPORT = {
  width: '100%',
  height: '100vh',
  latitude: 48.8029439,
  longitude: 2.485429,
  zoom: 13,
};

const MapPage = () => {
  const [state] = useContext(LocationDataContext);
  const [viewport, setViewport] = useState(DEFAULT_VIEWPORT);
  const listMarker = state.mainData.map((d) => (
    <MarkerDialog
      key={d.id}
      latitude={d.lat}
      longitude={d.long}
      data=""
      status="construction"
    ></MarkerDialog>
  ));

  return (
    <div>
      <ReactMapGl
        {...viewport}
        mapStyle="mapbox://styles/agillmann/ckgjesl9n19cg19mpqdy1x0pg"
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_API_KEY}
        onViewportChange={(viewport) => setViewport(viewport)}
      >
        {state.isLoading ? 'Loading' : listMarker}
      </ReactMapGl>
    </div>
  );
};

export default MapPage;
