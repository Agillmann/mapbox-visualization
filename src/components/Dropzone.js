import React, { useState, useContext } from 'react';
import fetchLocation from '../utils/fetchLocation';
import { CSVReader } from 'react-papaparse';

import { LocationDataContext } from '../GlobalState';

function Dropzone() {
  const [state, dispatch] = useContext(LocationDataContext);
  const [nbFetchComplete, setNbFetchComplete] = useState(0);
  const [nbFetchError, setNbFetchError] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [isFinish, setIsFinish] = useState(false);
  const [isComplete, setIsComplete] = useState(false);

  const handleOnDrop = (data) => {
    //console.log('---------------------------')
    dispatch({ type: 'REMOVE_DATA' });
    data.forEach(async (d) => {
      let response = await fetchLocation(d);
      setIsLoading(true);
      dispatch({ type: 'ADD_DATA', payload: response });
      dispatch({ type: 'ADD_NB_FETCH' });

      if (response) {
        setNbFetchComplete((nbFetchComplete) => nbFetchComplete + 1);
      } else {
        setNbFetchError((nbFetchError) => nbFetchError + 1);
      }
      setIsLoading(false);
      setIsFinish('true');
    });
  };
  if (nbFetchError > 0) {
    setIsComplete('true');
  }
  const handleOnError = (err, file, inputElem, reason) => {
    console.log(err);
  };

  const handleOnRemoveFile = (data) => {
    //console.log('---------------------------');
    dispatch({ type: 'RESET_LOCATION_DATA', payload: state.mainData });
    dispatch({ type: 'RESET_NB_FETCH' });
    setIsComplete(false);
    setIsFinish(false);
    setIsLoading(false);
    setNbFetchComplete(0);
    setNbFetchError(0);

    //console.log('---------------------------');
  };

  return (
    <div className="info info-container">
      <CSVReader
        onDrop={handleOnDrop}
        onError={handleOnError}
        addRemoveButton
        onRemoveFile={handleOnRemoveFile}
        style={{
          dropArea: {
            borderRadius: 20,
            height: 100,
          },
        }}
      >
        <span>Déposez le fichier csv ici ou cliquez pour télécharger</span>
      </CSVReader>
      <div></div>
      {isLoading && <span className="loading">Loading...</span>}
      {isFinish && (
        <div className="info">
          <span className="info_nbError">TOTAL : {nbFetchComplete}</span>
          <span className="info_nbComplete">Errors : {state.nbFetch - nbFetchComplete}</span>
        </div>
      )}
      {isComplete && <span className="info_isComplete">il manque des positions</span>}
    </div>
  );
}

export default Dropzone;
