import React from 'react';

// MATERIAL UI
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles({
  formControl: {
    width: '100%',
    paddingBottom: '1em',
  },
  inputFilters: {
    fontSize: '1,3em',
  },
  inputControl: {
    width: '100%',
  },
  menuItem: {
    padding: '1em',
  },
  selectItem: {},
});

const Filters = () => {
  const [filter, setFilter] = React.useState(0);
  console.log(filter);
  const handleChange = (event) => {
    setFilter(event.target.value);
  };

  const classes = useStyles();
  return (
    <FormControl className={classes.formControl}>
      <InputLabel className={classes.inputFilters} id="demo-simple-select-label">
        Filtres
      </InputLabel>
      <Select
        className={classes.selectItem}
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={filter}
        onChange={handleChange}
      >
        <MenuItem className={classes.menuItem} value={0}>
          Tout voir
        </MenuItem>
        <MenuItem value={1}>En cours de validation</MenuItem>
        <MenuItem value={2}>En construction</MenuItem>
        <MenuItem value={3}>Fini</MenuItem>
      </Select>
    </FormControl>
  );
};

export default Filters;
