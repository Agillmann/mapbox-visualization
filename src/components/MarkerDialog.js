import React from 'react';
import { Marker } from 'react-map-gl';

// MATERIAL UI
import { makeStyles } from '@material-ui/core/styles';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  div: {
    display: 'flex',
    justifyContent: 'flex-start',
  },
  section: {
    marginLeft: '1.5em',
  },
}));

const MarkerDialog = ({ latitude, longitude, data, status }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const matchColorStatus = (status) => {
    switch (status) {
      case 'construction':
        return 'red';
      case 'fini':
        return 'green';
      default:
        break;
    }
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  return (
    <React.Fragment>
      <Marker latitude={latitude} longitude={longitude}>
        <LocationOnIcon
          style={{ fontSize: 32, cursor: 'pointer', color: matchColorStatus(status) }}
          onClick={handleClickOpen}
        />
      </Marker>
      <Dialog
        fullWidth={true}
        maxWidth="md"
        open={open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="max-width-dialog-title">Name</DialogTitle>
        <DialogContent>
          <div className={classes.div}>
            <section className={classes.section}>
              <h3>Adresse : </h3>
              <p>status : </p>
              <p>telephone :</p>
            </section>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            X
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

MarkerDialog.propTypes = {
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  data: PropTypes.any,
  status: PropTypes.string,
};

export default MarkerDialog;
