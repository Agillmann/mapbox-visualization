import React, { useReducer, createContext } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
export const LocationDataContext = createContext();

const initialState = {
  mainData: [],
  isLoading: false,
  nbFetch: 0,
  nbFetchComplete: 0,
  lastUpdate: '',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      //console.log("==Global State==\n"state);
      return {
        ...state,
        lastUpdate: moment().format('llll'),
        mainData: [...state.mainData, action.payload],
      };
    case 'RESET_LOCATION_DATA':
      //console.log("==Global State==\n"state);
      return {
        ...state,
        mainData: [],
        isLoading: false,
        nbFetch: 0,
        nbFetchComplete: 0,
      };
    case 'ADD_NB_FETCH_COMPLETE':
      //console.log("==Global State==\n"state);
      return {
        ...state,
        nbFetchComplete: state.nbFetchComplete + 1,
      };
    case 'ADD_NB_FETCH':
      //console.log("==Global State==\n"state);
      return {
        ...state,
        nbFetch: state.nbFetch + 1,
      };
    case 'RESET_NB_FETCH':
      //console.log("==Global State==\n"state);
      return {
        ...state,
        nbFetch: 0,
        nbFetchComplete: 0,
      };
    default:
      return {
        ...state,
      };
  }
};

export const LocationDataContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <LocationDataContext.Provider value={[state, dispatch]}>
      {props.children}
    </LocationDataContext.Provider>
  );
};

LocationDataContextProvider.propTypes = {
  children: PropTypes.node,
};
